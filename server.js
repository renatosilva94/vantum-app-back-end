//Variáveis de Conexão

const mongoose = require('mongoose');
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const Data = require('./data');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();

//Link de Conexão
const dbRoute =
    'mongodb+srv://crud:96055450ab@vantum-3ne5f.mongodb.net/test?retryWrites=true&w=majority';

//Método para Conexão

mongoose.connect(dbRoute, { useNewUrlParser: true });

let db = mongoose.connection;

//Verificação de Conexão

db.once('open', () => console.log('Conectado a Base de Dados'));

db.on('error', console.error.bind(console, 'MongoDB connection error:'));



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

//GET DATA
router.get('/GET/user', (req, res) => {
    Data.find((err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data: data });
    });
});

//UPDATE DATA
router.put('/PUT/user', (req, res) => {
    const { id, update } = req.body;
    Data.findByIdAndUpdate(id, update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});

//DELETE DATA
router.delete('/DELETE/user', (req, res) => {
    const { id } = req.body;
    Data.findByIdAndRemove(id, (err) => {
        if (err) return res.send(err);
        return res.json({ success: true });
    });
});

//POST DATA
router.post('/POST/user', (req, res) => {
    let data = new Data();

    const { id, nome, email, endereco, telefone } = req.body;

    if ((!id && id !== 0) || !nome && !email && !telefone && !endereco) {
        return res.json({
            success: false,
            error: 'Campos Inválidos',
        });
    }
    data.nome = nome;
    data.email = email;
    data.telefone = telefone;
    data.endereco = endereco;
    data.id = id;
    data.save((err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});


app.use('/', router);


app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));